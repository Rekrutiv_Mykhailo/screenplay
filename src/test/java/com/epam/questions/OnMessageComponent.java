package com.epam.questions;

import com.epam.ui.MessagesComponent;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class OnMessageComponent {

  public static Question<Boolean> containMessage(String text) {

    return actor ->
        Text.of(MessagesComponent.MESSAGES).viewedBy(actor).asList().stream()
            .anyMatch(s -> s.equals(text));
  }
}
