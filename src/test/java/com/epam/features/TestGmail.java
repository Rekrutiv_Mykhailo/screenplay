package com.epam.features;

import static net.serenitybdd.screenplay.EventualConsequence.eventually;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

import com.epam.tasks.OpenTheApplication;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.questions.WebElementQuestion;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class TestGmail {

  Actor user = Actor.named("User");

  @Managed
  public WebDriver herBrowser;

  @Steps
  OpenTheApplication openTheApplication;

  @Before
  public void annaCanBrowseTheWeb() {
    user.can(BrowseTheWeb.with(herBrowser));
  }

  @Test
  public void the_user_can_login() {

    user.wasAbleTo(openTheApplication);

    user.attemptsTo(Click.on(By.cssSelector("a[class*='type_basket']")));

    user
        .should(eventually(
            seeThat(WebElementQuestion.the(By.cssSelector(".modal__holder")),
                isVisible())));
  }

  @Test
  public void the_user_can_login1() {

    user.wasAbleTo(openTheApplication);

    user.attemptsTo(Click.on(By.cssSelector("a[class*='type_basket']")));

    user
        .should(eventually(
            seeThat(WebElementQuestion.the(By.cssSelector(".modal__holder")),
                isVisible())));
  }

  @Test
  public void the_user_can_login2() {

    user.wasAbleTo(openTheApplication);

    user.attemptsTo(Click.on(By.cssSelector("a[class*='type_basket']")));

    user
        .should(eventually(
            seeThat(WebElementQuestion.the(By.cssSelector(".modal__holder")),
                isVisible())));
  }
}
