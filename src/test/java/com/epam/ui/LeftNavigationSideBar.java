package com.epam.ui;

import com.google.common.collect.ImmutableMap;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class LeftNavigationSideBar extends PageObject {

  private static Target COMPOSE_BUTTON =
      Target.the("Compose").located(By.xpath("//div[@jscontroller='DUNnfe']//div[@role='button']"));

  private static Target INBOX_MENU_ITEM =
      Target.the("Inbox").located(By.xpath("//div[@data-tooltip='Inbox']"));

  private static Target STARRED_MENU_ITEM =
      Target.the("Starred").located(By.xpath("//div[@data-tooltip='Starred']"));

  private static Target SNOOZED_MENU_ITEM =
      Target.the("Snoozed").located(By.xpath("//div[@data-tooltip='Snoozed']"));

  private static Target SENT_MENU_ITEM =
      Target.the("Sent").located(By.xpath("//div[@data-tooltip='Sent']"));

  private static Target DRAFTS_MENU_ITEM =
      Target.the("Drafts").located(By.xpath("//div[@data-tooltip='Drafts']"));

  private static Target MORE_MENU_ITEM =
      Target.the("More").located(By.xpath("//div[@data-tooltip='More']"));

  public static ImmutableMap<String, Target> leftNavigationElements =
      ImmutableMap.<String, Target>builder()
          .put(COMPOSE_BUTTON.getName(), COMPOSE_BUTTON)
          .put(INBOX_MENU_ITEM.getName(), INBOX_MENU_ITEM)
          .put(STARRED_MENU_ITEM.getName(), STARRED_MENU_ITEM)
          .put(SNOOZED_MENU_ITEM.getName(), SNOOZED_MENU_ITEM)
          .put(SENT_MENU_ITEM.getName(), SENT_MENU_ITEM)
          .put(DRAFTS_MENU_ITEM.getName(), DRAFTS_MENU_ITEM)
          .put(MORE_MENU_ITEM.getName(), MORE_MENU_ITEM)
          .build();
}
