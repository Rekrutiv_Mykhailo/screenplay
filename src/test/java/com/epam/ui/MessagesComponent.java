package com.epam.ui;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class MessagesComponent {
  public static Target MESSAGES =
      Target.the("messages")
          .located(
              By.xpath(
                  "//table[@role='grid']/tbody/tr[@role='row']//div[@role='link']/div/descendant::span[2]"));
  public static Target DELETE_MESSAGE_BUTTON =
      Target.the("delete").located(By.xpath("//div[@aria-label='Delete']"));
}
