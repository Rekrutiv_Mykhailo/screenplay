package com.epam.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class SendMessageModal extends PageObject {

  public static Target TO_FIELD = Target.the("To field").located(By.name("to"));
  public static Target SUBJECT_FIELD = Target.the("Subject field").located(By.name("subjectbox"));
  public static Target TEXT_AREA =
      Target.the("text area").located(By.xpath("//tbody//div[@role='textbox']"));
  public static Target SEND_BUTTON =
      Target.the("send button)")
          .located(
              By.xpath(
                  "//colgroup//following-sibling::tbody//div[@role='button' and @data-tooltip-delay]"));
}
