package com.epam.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class LogInGmailPage extends PageObject {
  public static Target LOGIN_FIELD = Target.the("login field").located(By.id("phone_email"));
  public static Target PASSWORD_FIELD = Target.the("password field").located(By.id("enterPassword"));
}
