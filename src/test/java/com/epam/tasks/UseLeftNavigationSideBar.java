package com.epam.tasks;

import com.epam.ui.LeftNavigationSideBar;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class UseLeftNavigationSideBar implements Task {

  private String menuItem;

  public UseLeftNavigationSideBar(String menuItem) {
    this.menuItem = menuItem;
  }

  @Override
  public <T extends Actor> void performAs(T actor) {
    actor.attemptsTo(Click.on(LeftNavigationSideBar.leftNavigationElements.get(menuItem)));
  }

  public static UseLeftNavigationSideBar navigateToPage(String menuItem) {
    return Tasks.instrumented(UseLeftNavigationSideBar.class, menuItem);
  }
}
