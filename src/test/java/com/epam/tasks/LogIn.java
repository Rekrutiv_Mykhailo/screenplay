package com.epam.tasks;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;

import com.epam.models.User;
import com.epam.ui.LogInGmailPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.EventualConsequence;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.questions.WebElementQuestion;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

public class LogIn implements Task {

  private LogInGmailPage logInGmailPage;

  private User user;

  public LogIn(User user) {
    this.user = user;
  }

  @Step("Log in gmail account")
  public <T extends Actor> void performAs(T actor) {
    enterUsername(actor);
    enterPassword(actor);
  }

  private <T extends Actor> void enterUsername(T actor) {
    actor.attemptsTo(Click.on(By.cssSelector("button[data-qaid='messages']")));
//
    actor.should(EventualConsequence
        .eventually(seeThat(WebElementQuestion.the(By.cssSelector("div[data-qaid='chat_header']")),
            WebElementStateMatchers.isVisible())));
  }

  private <T extends Actor> void enterPassword(T actor) {
    actor.attemptsTo(
        Enter.theValue(user.getPassword()).into(LogInGmailPage.PASSWORD_FIELD).thenHit(Keys.ENTER));
  }

  public static LogIn toGmailAccount(User user) {
    return Tasks.instrumented(LogIn.class, user);
  }
}
