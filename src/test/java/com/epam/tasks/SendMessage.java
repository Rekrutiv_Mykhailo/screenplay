package com.epam.tasks;

import com.epam.models.Message;
import com.epam.ui.LeftNavigationSideBar;
import com.epam.ui.SendMessageModal;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;

public class SendMessage implements Task {

  private Message message;

  public SendMessage(Message message) {
    this.message = message;
  }

  @Step("Send message")
  public <T extends Actor> void performAs(T actor) {
    clickComposeButton(actor);
    typeIntoToField(actor);
    typeIntoSubjectField(actor);
    typeIntoTextAreaField(actor);
    clickSendButton(actor);


  }

  public <T extends Actor> void clickComposeButton(T actor) {
    actor.attemptsTo(DeleteMessage.selectMessageByText(new Message("Test")));
    actor.attemptsTo(Click.on(LeftNavigationSideBar.leftNavigationElements.get("Compose")));
  }

  private  <T extends Actor> void clickSendButton(T actor) {
    actor.attemptsTo(Click.on(SendMessageModal.SEND_BUTTON));
  }

  private  <T extends Actor> void typeIntoSubjectField(T actor) {
    actor.attemptsTo(Enter.theValue(message.getSubject()).into(SendMessageModal.SUBJECT_FIELD));
  }

  private <T extends Actor> void typeIntoTextAreaField(T actor) {
    actor.attemptsTo(Enter.theValue(message.getBody()).into(SendMessageModal.TEXT_AREA));
  }

  private <T extends Actor> void typeIntoToField(T actor) {
    actor.attemptsTo(Enter.theValue(message.getReceiver()).into(SendMessageModal.TO_FIELD));
  }

  public static SendMessage to(Message message) {
    return Tasks.instrumented(SendMessage.class, message);
  }
}
