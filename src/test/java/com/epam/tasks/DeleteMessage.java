package com.epam.tasks;

import com.epam.models.Message;
import com.epam.ui.MessagesComponent;
import java.util.List;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import org.openqa.selenium.By;

public class DeleteMessage implements Task {

  private Message message;

  public DeleteMessage(Message message) {
    this.message = message;
  }

  @Override
  public <T extends Actor> void performAs(T actor) {
    BrowseTheWeb.as(actor)
        .findAll(
            (By.xpath(
                "//table[@role='grid']/tbody/tr[@role='row' and descendant::span[text() = 'Test2333']]//div[@role='checkbox']")))
        .get(0)
        .click();
    actor.attemptsTo(Click.on(MessagesComponent.DELETE_MESSAGE_BUTTON));

  }

  public static DeleteMessage selectMessageByText(Message message) {
    return Tasks.instrumented(DeleteMessage.class, message);
  }
}
