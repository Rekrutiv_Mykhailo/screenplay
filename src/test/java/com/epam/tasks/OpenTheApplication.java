package com.epam.tasks;

import com.epam.ui.PromHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;

public class OpenTheApplication implements Task {

  private PromHomePage promHomePage;

  @Step("Open the application")
  public <T extends Actor> void performAs(T actor) {
    actor.attemptsTo(Open.browserOn().the(promHomePage));
  }
}
